package hotel;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

public class Booking {
    private int id;
    private LocalDate checkIn;
    private LocalDate checkOut;
    private Customer customer;
    private Room room;
    private Boolean canceled = false;
    private static int counterBooking;

    public Booking(LocalDate checkIn, LocalDate checkOut,Customer customer, Room room){
        this.id = ++ counterBooking;
        this.checkIn = checkIn;
        this.checkOut = checkOut;
        this.customer = customer;
        this.room = room;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public LocalDate getCheckIn() {
        return checkIn;
    }

    public void setCheckIn(LocalDate checkIn) {
        this.checkIn = checkIn;
    }

    public LocalDate getCheckOut() {
        return checkOut;
    }

    public void setCheckOut(LocalDate checkOut) {
        this.checkOut = checkOut;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public Boolean isCanceled() {
        return canceled;
    }

    public void setCanceled() {
        this.canceled = true;
    }

    //Calculate days between checkIn and Checkout
    public long countDays(){
        long p2 = ChronoUnit.DAYS.between(this.checkIn,this.checkOut);
        if (p2 == 0){
            p2 = 1;
        }
        return p2;
    }

    //Return price to pay for the booking dates.
    public double getPrice(){
        double price = room.getPrice();
        price = price * countDays();
        if (countDays() > 7){
            if (this.room instanceof DeluxeRoom){
                return price * 0.90;
            }else{
                return price * 0.95;
            }
        }
        else{
            return price;
        }
    }


    //return true if checkin or checkout is bewteen dates
    public boolean bookingBetweenDays(LocalDate date1, LocalDate date2){
        return (date1.isBefore(this.checkIn) || date2.isAfter(this.checkOut)
                || date1.isEqual(this.checkIn)||date2.isEqual(this.checkOut));
    }

    //return true if bookings is in course
    public boolean bookingInCourse() {
        LocalDate date = LocalDate.now();
        return (date.isAfter(this.checkIn) && date.isBefore(this.checkOut)
                || date.isEqual(this.checkIn)||date.isEqual(this.checkOut));
    }

    @Override
    public String toString(){
        return "Booking ID: " + this.id + " | Customer name: " + this.customer.getName() + " | Room number: " +
                this.room.getNumber() + " | Check In date: " + this.checkIn + " | Check Out date: " + this.checkOut +
                " | Total days: " + this.countDays() + " | Total amount: U$S" + this.getPrice();
    }
}


