package hotel;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;

public class HotelSystem {
    private HashMap<Integer,Booking> bookings;
    private HashMap<String,Customer> customers;
    private HashMap<Integer,Room> rooms;

    private static HotelSystem hotelSystem = new HotelSystem();

    private HotelSystem(){
        this.bookings = new HashMap<>();
        this.customers = new HashMap<>();
        this.rooms = new HashMap<>();
    }

    public static HotelSystem getInstance(){
        return hotelSystem;
    }

    public HashMap<Integer, Booking> getBookingsFromTo() {
        return bookings;
    }

    public void setBookings(HashMap<Integer, Booking> bookings) {
        this.bookings = bookings;
    }

    public HashMap<String, Customer> getCustomers() {
        return customers;
    }

    public void setCustomers(HashMap<String, Customer> customers) {
        this.customers = customers;
    }

    public HashMap<Integer, Room> getRooms() {
        return rooms;
    }

    public void setRooms(HashMap<Integer, Room> rooms) {
        this.rooms = rooms;
    }

    //Return if costumer already exists or not.
    public boolean isCustomerIn(Customer customer){
        return this.customers.containsKey(customer.getId());
    }

    //Costumer doesn't exist
    public void addCostumer(Customer customer){
        this.customers.put(customer.getId(),customer);
    }


    //Return bookings between dates.
    public ArrayList<Booking> getBookingsFromTo(LocalDate fromDate, LocalDate toDate){
        ArrayList<Booking> bookingFromTo = new ArrayList<Booking>();
        for (Booking booking: this.bookings.values()){
            if (booking.bookingBetweenDays(fromDate,toDate)){
                bookingFromTo.add(booking);
            }
        }
        return bookingFromTo;
    }

    //Check if room is available to be booked
    private boolean checkRoomAvailabilty(Room room, LocalDate checkIn, LocalDate checkOut){
        boolean available = true;
        ArrayList<Booking> bookings = getBookingsFromTo(checkIn,checkOut);
        for(Booking book:bookings){
            if (book.getRoom() == room){
                available = false;
                break;
            }
        }
        return available;
    }
    //add booking
    public void book(Room room,Customer customer,LocalDate checkIn, LocalDate checkOut){
        if (checkRoomAvailabilty(room,checkIn,checkOut)){
            Booking booking = new Booking(checkIn,checkOut,customer,room);
            this.bookings.put(booking.getId(),booking);
        }
        else{
            System.out.println("Room is already booked");
        }
    }

    public void cancelBooking(int numBooking){
        try {
            if (!this.bookings.get(numBooking).bookingInCourse()) {
                if (!this.bookings.get(numBooking).isCanceled()) {
                    this.bookings.get(numBooking).setCanceled();
                } else {
                    System.out.println("This booking is already canceled");
                }
            } else {
                System.out.println("Can't be cancelled, this booking is already in course.");
            }
        } catch (Exception e){
                System.out.printf("Booking doesn't exists.\n");
            }
    }
}



