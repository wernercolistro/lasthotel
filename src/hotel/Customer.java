package hotel;

public class Customer {
    private String name;
    private String id;
    private String phoneNumber;

    public Customer(String name, String id, String telNumber){
        this.name = name;
        this.id = id;
        this.phoneNumber = telNumber;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTelNumber() {
        return phoneNumber;
    }

    public void setTelNumber(String telNumber) {
        this.phoneNumber = telNumber;
    }

    @Override
    public String toString(){
        return "Name: " + this.name + " | id: " + this.id + " | Phone: " + this.phoneNumber;
    }
}


