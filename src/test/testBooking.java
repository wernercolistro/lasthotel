package test;
import hotel.*;

import java.time.LocalDate;
import java.util.ArrayList;


public class testBooking {

    public static void main(String[] args) {
        HotelSystem hotelSystem = HotelSystem.getInstance();

        LocalDate checkIn = LocalDate.of(2021,10,10);
        LocalDate checkOut = LocalDate.of(2021,10,30);
        LocalDate date1 = LocalDate.of(2020,01,01);
        LocalDate date2 = LocalDate.of(2022, 01, 01);
        LocalDate date3init = LocalDate.of(2021,10,15);
        LocalDate date3final = LocalDate.of(2021,10, 20);

        Room room1 = new Room(1);
        Customer cust1 = new Customer("Werner Colistro","50288260","098275321");
        DeluxeRoom roomDeluxe1 = new DeluxeRoom(2);

        hotelSystem.book(room1,cust1,checkIn,checkOut);
        hotelSystem.book(roomDeluxe1,cust1,checkIn,checkOut);
        hotelSystem.book(room1,cust1,date3init,date3final);


        ArrayList<Booking> testBooking = new ArrayList<>();
        testBooking = hotelSystem.getBookingsFromTo(date1,date2);
        for (int i=0; i < testBooking.size();i++){
            System.out.println(testBooking.get(i));
        }
        hotelSystem.cancelBooking(1);
        hotelSystem.cancelBooking(5);
        hotelSystem.cancelBooking(1);

    }

}
