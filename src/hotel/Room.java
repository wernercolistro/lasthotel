package hotel;


import java.util.ArrayList;

public class Room {
    protected static enum Amenity {
        MINIBAR,
        SAFE_BOX,
        WIFI,
        ROOM_SERVICE,
        LAUNDRY_SERVICE,
        CLEANING_SERVICE,
    }

    //Standard Room
    protected int number;
    protected ArrayList<Amenity> amenities;
    private double price;
    private boolean available;

    public Room(int number){
        this.number = number;
        this.price = 50;
        amenities = new ArrayList<Amenity>();
        this.amenities.add(Amenity.WIFI);
        this.amenities.add(Amenity.ROOM_SERVICE);
        this.amenities.add(Amenity.CLEANING_SERVICE);
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getNumber() {
        return number;
    }

    public ArrayList<Amenity> getAmenities() {
        return amenities;
    }

    public void setAmenities(ArrayList<Amenity> amenities) {
        this.amenities = amenities;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }


}


