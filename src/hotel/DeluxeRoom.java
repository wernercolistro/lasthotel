package hotel;


public class DeluxeRoom extends Room{

    public DeluxeRoom(int number){
        super(number);
        this.amenities.add(Room.Amenity.MINIBAR);
        this.amenities.add(Room.Amenity.SAFE_BOX);
        this.amenities.add(Room.Amenity.LAUNDRY_SERVICE);
        this.setPrice(90);
    }
}

